import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { LoginComponent } from './login/login.component';
import { SuccesComponent } from './login/succes/succes.component';
import { DesignModule } from './design/design.module';

const routes: Routes = [
  // {
  //   path: '',
  //   component: HomeComponent,
  //   pathMatch: 'full',
  // },
  {
    path: 'auth',
    component: LoginComponent,
  },
  {
    path: 'auth/succes/:jwt',
    component: SuccesComponent,
  },
  {
    path: 'designs',
    loadChildren: () => DesignModule,
    // loadChildren: () =>
    //   import('./design/design.module').then((m) => m.DesignModule),
  },
  {
    path: '**',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
