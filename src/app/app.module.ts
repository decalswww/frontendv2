import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { ToolbarModule } from './toolbar/toolbar.module';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home.component';
import { LoginComponent } from './login/login.component';
import { SuccesComponent } from './login/succes/succes.component';
import { DesignModule } from './design/design.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [AppComponent, HomeComponent, LoginComponent, SuccesComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    ToolbarModule,
    CoreModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    DesignModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
