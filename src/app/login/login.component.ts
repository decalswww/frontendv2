import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  private googleLogoURL = '../assets/Google__G__Logo.svg';
  private discordLogoURL = '../assets/Discord_Logo.svg';

  public googleOauthUrl = `${environment.apiUrl}/auth/google`;
  public discordOauthUrl = `${environment.apiUrl}/auth/discord`;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
  ) {
    this.matIconRegistry.addSvgIcon(
      'google',
      this.domSanitizer.bypassSecurityTrustResourceUrl(this.googleLogoURL),
    );

    this.matIconRegistry.addSvgIcon(
      'discord',
      this.domSanitizer.bypassSecurityTrustResourceUrl(this.discordLogoURL),
    );
  }

  loginGoogle(): void {
    window.location.href = this.googleOauthUrl;
  }

  loginDiscord(): void {
    window.location.href = this.discordOauthUrl;
  }
}
