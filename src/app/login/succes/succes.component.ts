import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-succes',
  templateUrl: './succes.component.html',
  styleUrls: ['./succes.component.scss'],
})
export class SuccesComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    const jwt = this.route.snapshot.paramMap.get('jwt');
    if (jwt) {
      this.authService.login(jwt);
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['auth']);
    }
  }
}
