import {
  Component,
  Output,
  EventEmitter,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, map, distinctUntilChanged } from 'rxjs/operators';
import { TagService } from '../tag.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ChangeDetectorRef } from '@angular/core';

export interface FilterDto {
  search: string;
  tagSearch: string;
  offset: string;
  limit: string;
  sort: string;
}

@Component({
  selector: 'app-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.css'],
})
export class FilterFormComponent implements OnDestroy {
  filterForm = this.formBuilder.group({
    search: [''],
    tagAutoComplete: [''],
  });

  @Output() searchChanged = new EventEmitter<string>();
  @Output() tagChanged = new EventEmitter<string>();
  private subscriptions: Subscription = new Subscription();
  tags: string[] = [];
  selectedTags: string[] = [];

  @ViewChild('tagInput') tagInput?: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private tagService: TagService,
    private cd: ChangeDetectorRef,
  ) {
    const searchField = this.filterForm.get('search');
    if (searchField) {
      this.subscriptions.add(
        searchField.valueChanges
          .pipe(
            debounceTime(500),
            map((value: string) => value.trim()),
            distinctUntilChanged(),
          )
          .subscribe((search) => {
            this.searchChanged.emit(search);
          }),
      );
    }

    const tagField = this.filterForm.get('tagAutoComplete');
    if (tagField) {
      this.subscriptions.add(
        tagField.valueChanges
          .pipe(debounceTime(350), distinctUntilChanged())
          .subscribe((res) => this.getTags(res)),
      );
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  getTags(search: string): void {
    if (search.trim().length > 0) {
      this.subscriptions.add(
        this.tagService.searchTag(search).subscribe((newTags) => {
          this.tags = newTags;
        }),
      );
    }
  }

  removeTag(tag: string): void {
    const index = this.selectedTags.indexOf(tag);
    if (index >= 0) {
      this.selectedTags.splice(index, 1);
    }

    this.f.tagAutoComplete.enable();
    this.tagChanged.emit('');
  }

  selectedTag(event: MatAutocompleteSelectedEvent): void {
    const tag = event.option.viewValue;
    this.selectedTags.push(tag);

    if (this.tagInput) {
      this.tagInput.nativeElement.value = '';
    }
    this.f.tagAutoComplete.disable();
    this.tagChanged.emit(tag);
  }

  overrideTag(tag: string): void {
    if (tag && tag !== '') {
      this.selectedTags = [tag];
      this.f.tagAutoComplete.disable();
    } else {
      this.selectedTags = [];
      this.f.tagAutoComplete.enable();
    }

    this.cd.detectChanges();
  }

  get f(): {
    [key: string]: AbstractControl;
  } {
    return this.filterForm.controls;
  }
}
