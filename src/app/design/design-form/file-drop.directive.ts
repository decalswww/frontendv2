import {
  Directive,
  Output,
  EventEmitter,
  HostBinding,
  HostListener,
  ElementRef,
} from '@angular/core';

@Directive({
  selector: '[appDragDrop]',
})
export class DragDropDirective {
  @Output() FileDropped = new EventEmitter<FileList>();

  @HostBinding('style.background-color') private background: string;
  @HostBinding('style.opacity') private opacity = '1';

  private originalBackground: string;

  constructor(el: ElementRef) {
    this.background = el.nativeElement.style.background;
    this.originalBackground = this.background;
  }

  //Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#9ecbec';
    this.opacity = '0.8';
  }

  //Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(
    evt: DragEvent,
  ): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = this.originalBackground;
    this.opacity = '1';
  }

  //Drop listener
  @HostListener('drop', ['$event']) public ondrop(evt: DragEvent): void {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = this.originalBackground;
    this.opacity = '1';
    const files = evt.dataTransfer?.files;
    if (files && files.length > 0) {
      this.FileDropped.emit(files);
    }
  }
}
