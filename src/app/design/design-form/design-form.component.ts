import { Component, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DesignService } from '../design.service';
import Design from 'src/app/core/interfaces/design.interface';
import { AuthService } from 'src/app/core/services/auth.service';
import { HttpEventType, HttpEvent } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { CreateDesignDto } from '../dto/create-design.dto';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import Tag from 'src/app/core/interfaces/tag.interface';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TagService } from '../tag.service';
import { MatChipInputEvent } from '@angular/material/chips';

function isFileImage(file: File): boolean {
  return file && file.type.split('/')[0] === 'image';
}

function isFileZip(file: File): boolean {
  return file && file.type.split('/')[1] === 'x-zip-compressed';
}

@Component({
  selector: 'app-design-form',
  templateUrl: './design-form.component.html',
  styleUrls: ['./design-form.component.css'],
})
export class DesignFormComponent implements OnDestroy {
  designForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    description: [''],
    thumbnail: [null, Validators.required],
    thumbnailLbl: ['', Validators.required],
    zip: [null, Validators.required],
    zipLbl: ['', Validators.required],
    tagAutoComplete: [''],
  });

  previewDesign: Design = {
    id: 0,
    creator: { id: 0, name: '', avatar: '' },
    name: '',
    description: '',
    thumbnailDataUrl: '',
    date: 0,
    downloads: 0,
    tags: [],
  };

  thumbnailPreview = '';

  uploadProgress = 0;
  private mode: 'edit' | 'post' = 'post';
  private subscriptions: Subscription;
  autoCompleteTags: string[] = [];
  separatorKeysCodes: number[] = [ENTER, COMMA];

  @ViewChild('tagInput') tagInput?: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private designService: DesignService,
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private tagService: TagService,
  ) {
    this.subscriptions = this.authService.userFetched$.subscribe((user) => {
      if (user) {
        this.previewDesign.creator = user;
      }
    });

    this.designForm.get('name')?.valueChanges.subscribe((val) => {
      this.previewDesign.name = val;
    });

    this.designForm.get('description')?.valueChanges.subscribe((val) => {
      this.previewDesign.description = val;
    });

    this.designForm.get('thumbnailLbl')?.valueChanges.subscribe(() => {
      this.previewDesign.thumbnailDataUrl = this.thumbnailPreview;
    });

    const tagField = this.designForm.get('tagAutoComplete');
    if (tagField) {
      this.subscriptions.add(
        tagField.valueChanges
          .pipe(debounceTime(350), distinctUntilChanged())
          .subscribe((res) => this.getTags(res)),
      );
    }
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  submit(): void {
    if (!this.designForm.valid) {
      console.log('Invalid form', this.designForm);
      return;
    }
    if (this.mode === 'edit') {
      this.patch();
    } else {
      this.designService.postDesign(this.getDto()).subscribe(
        (evt) => this.onUploadProgress(evt, 'Design Posted!'),
        (err) => {
          console.log(err);
        },
      );
    }
  }

  patch(): void {
    this.designService
      .patchDesign(this.previewDesign.id, this.getDto())
      .subscribe(
        (evt) => this.onUploadProgress(evt, 'Design Updated!'),
        (err) => {
          console.log(err);
        },
      );
  }

  getDto(): CreateDesignDto {
    console.log('previewDesign.tags', this.previewDesign.tags);

    return {
      name: this.designForm.value.name,
      description: this.designForm.value.description,
      zipFile: this.designForm.value.zip,
      thumbnailFile: this.designForm.value.thumbnail,
      tags: this.previewDesign.tags.map((t) => t.name),
    };
  }

  setMode(mode: 'edit' | 'post'): void {
    if (mode === this.mode) {
      return;
    }
    this.mode = mode;

    if (mode === 'edit') {
      this.designForm.get('zip')?.clearValidators();
      this.designForm.get('zipLbl')?.clearValidators();
      this.designForm.get('thumbnailLbl')?.clearValidators();
      this.designForm.get('thumbnail')?.clearValidators();
    } else {
      this.designForm.get('zip')?.setValidators(Validators.required);
      this.designForm.get('zipLbl')?.setValidators(Validators.required);
      this.designForm.get('thumbnailLbl')?.setValidators(Validators.required);
      this.designForm.get('thumbnail')?.setValidators(Validators.required);
    }
    this.designForm.get('zip')?.updateValueAndValidity();
    this.designForm.get('zipLbl')?.updateValueAndValidity();
    this.designForm.get('thumbnailLbl')?.updateValueAndValidity();
    this.designForm.get('thumbnail')?.updateValueAndValidity();
  }

  onFileDrop(fileList: FileList): void {
    for (let index = 0; index < fileList.length; index++) {
      const file = fileList[index];
      console.log(file);

      if (isFileImage(file)) {
        this.setThumbnail(file);
      } else if (isFileZip(file)) {
        this.setZip(file);
      }
    }
  }

  private setThumbnail(file: File): void {
    this.designForm.patchValue({ thumbnail: file });

    const reader = new FileReader();
    reader.onload = (): void => {
      this.thumbnailPreview = reader.result as string;
      const img = new Image();
      img.onload = (): void => {
        this.designForm.patchValue({
          thumbnailLbl: `${file.name} (${img.width}x${img.height}, ${Math.round(
            file.size / 1024,
          )}kb)`,
        });
      };
      img.src = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  private setZip(file: File): void {
    this.designForm.patchValue({ zip: file });
    this.designForm.patchValue({
      zipLbl: `${file.name} (${Math.round(file.size / 1024)}kb)`,
    });
  }

  onThumbnailChange(evt: Event): void {
    const input = evt.target as HTMLInputElement;
    if (input && input.files) {
      this.setThumbnail(input.files[0]);
    }
  }

  onZipChange(evt: Event): void {
    const input = evt.target as HTMLInputElement;
    if (input && input.files) {
      this.setZip(input.files[0]);
    }
  }

  getTags(search: string): void {
    if (search && search.trim().length > 0) {
      this.subscriptions.add(
        this.tagService.searchTag(search).subscribe((newTags) => {
          this.autoCompleteTags = newTags;
        }),
      );
    }
  }

  removeTag(tag: string): void {
    const newTags = this.previewDesign.tags.filter((t) => t.name !== tag);
    this.previewDesign.tags = newTags;
    // const index = this.previewDesign.tags.indexOf(tag);
    // if (index >= 0) {
    //   this.selectedTags.splice(index, 1);
    // }

    // this.f.tagAutoComplete.enable();
    // this.tagChanged.emit('');
  }

  addTag(event: MatAutocompleteSelectedEvent): void {
    const tag = event.option.viewValue;
    console.log(tag);
    this.previewDesign.tags.push({ name: tag, id: 0 });

    if (this.tagInput) {
      this.tagInput.nativeElement.value = '';
    }
  }

  addNewTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.previewDesign.tags.push({ name: value.trim(), id: 0 });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    // this.fruitCtrl.setValue(null);
  }

  onUploadProgress(event: HttpEvent<Design>, finishedMessage: string): void {
    switch (event.type) {
      case HttpEventType.Sent:
        this.uploadProgress = 0.01;
        break;
      case HttpEventType.UploadProgress:
        this.uploadProgress = Math.round(
          (event.loaded / (event.total || 1)) * 100,
        );
        break;
      case HttpEventType.Response:
        this._snackBar.open(finishedMessage, 'Fantastic', {
          duration: 5000,
        });
        this.designForm.reset();
        this.previewDesign.tags = [];
        Object.keys(this.designForm.controls).forEach((key) => {
          this.designForm.get(key)?.setErrors(null);
        });
        setTimeout(() => {
          this.uploadProgress = 0;
        }, 1500);
    }
  }

  // convenience getter for easy access to form fields
  get f(): {
    [key: string]: AbstractControl;
  } {
    return this.designForm.controls;
  }
}
