import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import DesignJsonConfig from './jsonConfig';

@Component({
  selector: 'app-design-json-creator',
  templateUrl: './design-json-creator.component.html',
  styleUrls: ['./design-json-creator.component.css'],
})
export class DesignJsonCreatorComponent {
  jsonForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(2)]],
    BodyID: ['', [Validators.pattern('^[0-9]*$')]],
    SkinID: ['', [Validators.pattern('^[0-9]*$')]],
    ChassisDiffuse: [''],
    BodyDiffuse: [''],
    BodySkin: [''],
  });

  jsonConfig: Map<string, DesignJsonConfig> = new Map<
    string,
    DesignJsonConfig
  >();
  jsonConfigKeys: string[];
  jsonOutput = '';

  constructor(private formBuilder: FormBuilder) {
    this.jsonConfigKeys = [];
  }

  remove(name: string): void {
    this.jsonConfig.delete(name);
    this.jsonConfigKeys = this.jsonConfigKeys.filter((k) => k !== name);
    this.updateOutput();
  }

  edit(name: string): void {
    const config = this.jsonConfig.get(name);
    if (config) {
      this.setForm(name, config);
    }
  }

  add(name: string, config: DesignJsonConfig): void {
    this.jsonConfig.set(name, config);
    this.updateOutput();
    if (this.jsonConfigKeys.findIndex((v) => v === name) === -1) {
      this.jsonConfigKeys.push(name);
    }
  }

  readForm(): { name: string; config: DesignJsonConfig } {
    const name = this.f.name.value;
    const config: DesignJsonConfig = {
      BodyID: +this.f.BodyID.value || undefined,
      SkinID: +this.f.SkinID.value || undefined,
      Chassis: {
        Diffuse: this.f.ChassisDiffuse.value || undefined,
      },
      Body: {
        Diffuse: this.f.BodyDiffuse.value || undefined,
        Skin: this.f.BodySkin.value || undefined,
      },
    };
    return { name, config };
  }

  setForm(name: string, config: DesignJsonConfig): void {
    this.jsonForm.patchValue({
      name,
      BodyID: config.BodyID || undefined,
      SkinID: config.SkinID || undefined,
      ChassisDiffuse: config.Chassis?.Diffuse || undefined,
      BodyDiffuse: config.Body?.Diffuse || undefined,
      BodySkin: config.Body?.Skin || undefined,
    });
  }

  submit(): void {
    if (this.jsonForm.valid) {
      const { name, config } = this.readForm();
      this.add(name, config);
    }
  }

  reset(): void {
    this.jsonOutput = '';
    this.jsonConfigKeys = [];
    this.jsonConfig.clear();
  }

  updateOutput(): void {
    const record: Record<string, DesignJsonConfig> = {};
    this.jsonConfig.forEach((v, k) => {
      record[k] = v;
    });
    this.jsonOutput = this.syntaxHighlight(JSON.stringify(record, null, 2));
  }

  // convenience getter for easy access to form fields
  get f(): {
    [key: string]: AbstractControl;
  } {
    return this.jsonForm.controls;
  }

  PromotForJson(): void {
    const jsonRaw = prompt('Paste in your current JSON', '');
    if (jsonRaw) {
      this.reset();
      const json = JSON.parse(jsonRaw);
      Object.keys(json).forEach((key) => {
        this.add(key, json[key]);
      });
    }
  }
  // https://stackoverflow.com/questions/4810841/pretty-print-json-using-javascript
  syntaxHighlight(json: string): string {
    json = json
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
    return json.replace(
      /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
      (match): string => {
        let cls = 'number';
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = 'key';
          } else {
            cls = 'string';
          }
        } else if (/true|false/.test(match)) {
          cls = 'boolean';
        } else if (/null/.test(match)) {
          cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
      },
    );
  }
}
