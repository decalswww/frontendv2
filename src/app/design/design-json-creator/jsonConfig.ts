export default interface DesignJsonConfig {
  BodyID?: number;
  SkinID?: number;
  Chassis: {
    Diffuse?: string;
  };
  Body: {
    Diffuse?: string;
    Skin?: string;
  };
}
