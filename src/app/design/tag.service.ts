import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of, concat } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import Tag from 'src/app/core/interfaces/tag.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TagService {
  private tagCache: string[] = [];

  constructor(private http: HttpClient) {}

  searchTag(search: string): Observable<string[]> {
    const cache = of(this.filterCache(search));
    const apiResult = this.http
      .get<Tag[]>(`${environment.apiUrl}/tag/search/${search}`)
      .pipe(map((tags) => tags.map((tag) => tag.name)));

    const combined = concat(cache, apiResult);
    apiResult.subscribe((newTags) => this.addToCache(newTags));

    return combined;
  }

  filterCache(search: string): string[] {
    return this.tagCache.filter((t) =>
      t.toLowerCase().includes(search.toLowerCase()),
    );
  }

  addToCache(tags: string[]): void {
    this.tagCache = [...new Set(tags.concat([...this.tagCache]))];
  }
}
