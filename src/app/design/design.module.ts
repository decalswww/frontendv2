import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DesignRoutingModule } from './design-routing.module';
import { DesignItemComponent } from './design-item/design-item.component';
import { DesignGridComponent } from './design-grid/design-grid.component';
import { DesignFormComponent } from './design-form/design-form.component';
import { DragDropDirective } from './design-form/file-drop.directive';
import { DesignEditorComponent } from './design-editor/design-editor.component';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { FilterFormComponent } from './filter-form/filter-form.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MaterialElevationDirective } from './hover-elevation.directive';
import { DesignJsonCreatorComponent } from './design-json-creator/design-json-creator.component';

@NgModule({
  declarations: [
    DesignItemComponent,
    DesignGridComponent,
    DesignFormComponent,
    DragDropDirective,
    DesignEditorComponent,
    FilterFormComponent,
    MaterialElevationDirective,
    DesignJsonCreatorComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    DesignRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
  ],
  exports: [DesignItemComponent],
})
export class DesignModule {}
