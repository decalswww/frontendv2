export interface CreateDesignDto {
  name: string;

  description: string;

  zipFile: File;

  thumbnailFile: File;

  tags: string[];
}
