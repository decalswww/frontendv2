import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DesignService } from '../design.service';
import { Subscription } from 'rxjs';
import { DesignFormComponent } from '../design-form/design-form.component';
import Design from 'src/app/core/interfaces/design.interface';

@Component({
  selector: 'app-design-editor',
  templateUrl: './design-editor.component.html',
  styleUrls: ['./design-editor.component.css'],
})
export class DesignEditorComponent implements OnInit, OnDestroy {
  private designSubscription?: Subscription;
  @ViewChild(DesignFormComponent)
  private formComp?: DesignFormComponent;

  constructor(
    private route: ActivatedRoute,
    private designService: DesignService,
  ) {}

  ngOnDestroy(): void {
    this.designSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    const designId = this.route.snapshot.paramMap.get('id');
    if (designId) {
      this.designSubscription = this.designService
        .getDesign(+designId)
        .subscribe((design) => {
          this.setFormToEdit(design);
        });
    }
  }

  setFormToEdit(design: Design): void {
    if (this.formComp) {
      console.log('design to edit:', design);
      this.formComp.setMode('edit');
      this.formComp.designForm.patchValue(design);
      this.formComp.previewDesign = design;
    }
  }
}
