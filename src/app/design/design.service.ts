import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import Design from '../core/interfaces/design.interface';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../core/services/auth.service';

import { CreateDesignDto } from './dto/create-design.dto';
import { FilterDto } from './filter-form/filter-form.component';

@Injectable({
  providedIn: 'root',
})
export class DesignService {
  designsCache: Design[] = [];
  designs$ = new BehaviorSubject<Design[] | null>(null);
  top5$ = new BehaviorSubject<Design[] | null>(null);
  last5$ = new BehaviorSubject<Design[] | null>(null);
  fetching$ = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchDesigns(): void {
    // this.designs$.next([]);
    this.fetching$.next(true);
    this.http
      .get<Design[]>(`${environment.apiUrl}/designs`)
      .subscribe((res) => {
        // console.log(res);
        this.designs$.next(res);
        this.fetching$.next(false);
      });
  }

  getDesignsFiltered(filter: FilterDto): void {
    if (filter.search === '') delete filter.search;
    if (filter.tagSearch === '') delete filter.tagSearch;
    this.designs$.next([]);

    this.fetching$.next(true);
    this.http
      .get<Design[]>(`${environment.apiUrl}/designs`, {
        params: { ...filter },
      })
      .subscribe((designs) => {
        // console.log(designs);
        this.designs$.next(designs);
        this.fetching$.next(false);
      });
  }

  getDesignsFilterdAsObservable(filter: FilterDto): Observable<Design[]> {
    if (filter.search === '') delete filter.search;
    if (filter.tagSearch === '') delete filter.tagSearch;

    return this.http.get<Design[]>(`${environment.apiUrl}/designs`, {
      params: { ...filter },
    });
  }

  fetchTop5(): void {
    const filter: FilterDto = {
      search: '',
      tagSearch: '',
      offset: '0',
      limit: '5',
      sort: 'downloads:DESC',
    };
    this.getDesignsFilterdAsObservable(filter).subscribe((designs) => {
      console.log('top 5:', designs);
      this.top5$.next(designs);
    });
  }

  fetchLast5(): void {
    const filter: FilterDto = {
      search: '',
      tagSearch: '',
      offset: '0',
      limit: '5',
      sort: 'updated:DESC',
    };
    this.getDesignsFilterdAsObservable(filter).subscribe((designs) => {
      console.log('last 5:', designs);
      this.last5$.next(designs);
    });
  }

  getDesign(id: number): Observable<Design> {
    return this.http.get<Design>(`${environment.apiUrl}/designs/${id}`);
  }

  async deleteDesign(design: Design): Promise<void> {
    if (
      !confirm(
        'Are you sure you want to delete this? (Unreversible!) TODO: use a custom dialog and move the button to the details page',
      )
    ) {
      return;
    }

    const headers = this.GetAuthHeaders();

    this.http
      .delete(`${environment.apiUrl}/designs/${design.id}`, { headers })
      .subscribe(
        () => {
          const oldDesigns = this.designs$.getValue();
          if (oldDesigns) {
            const filteredDesigns = oldDesigns.filter(
              (d) => d.id !== design.id,
            );
            this.designs$.next(filteredDesigns);
          }
        },
        (err) => {
          console.log('Error', err);
        },
      );
  }

  private GetAuthHeaders(): HttpHeaders {
    return new HttpHeaders({
      // 'Content-Type': 'application/json',
      Authorization: `Bearer ${this.authService.getToken()}`,
    });
  }

  postDesign({
    name,
    description,
    zipFile,
    thumbnailFile,
    tags,
  }: CreateDesignDto): Observable<HttpEvent<Design>> {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('zip', zipFile);
    formData.append('thumbnail', thumbnailFile);
    if (tags && tags.length > 0) {
      formData.append('tags', JSON.stringify(tags));
    }

    const headers = this.GetAuthHeaders();
    return this.http.post<Design>(`${environment.apiUrl}/designs`, formData, {
      headers,
      reportProgress: true,
      observe: 'events',
    });
  }

  patchDesign(
    id: number,
    { name, description, zipFile, thumbnailFile, tags }: CreateDesignDto,
  ): Observable<HttpEvent<Design>> {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('description', description);
    formData.append('tags', JSON.stringify(tags));
    if (zipFile) {
      formData.append('zip', zipFile);
    }
    if (thumbnailFile) {
      formData.append('thumbnail', thumbnailFile);
    }

    const headers = this.GetAuthHeaders();
    return this.http.patch<Design>(
      `${environment.apiUrl}/designs/${id}`,
      formData,
      {
        headers,
        reportProgress: true,
        observe: 'events',
      },
    );
  }
}
