import { Component, Input, Output, EventEmitter } from '@angular/core';
import Design from '../../core/interfaces/design.interface';
import { DesignService } from 'src/app/design/design.service';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/core/interfaces/user.interface';
import { Observable } from 'rxjs';
import Tag from 'src/app/core/interfaces/tag.interface';

function isValidUrl(str: string): boolean {
  try {
    new URL(str);
  } catch (_) {
    return false;
  }

  return true;
}

function toHttps(url: string): string {
  try {
    const safeURL = new URL(url);
    safeURL.protocol = 'https:';
    return safeURL.toString() + '/';
  } catch (_) {
    return '';
  }
}

@Component({
  selector: 'app-design-item[designItem]', // [designItem] makes it a required input. Checked at compile time
  templateUrl: './design-item.component.html',
  styleUrls: ['./design-item.component.css'],
})
export class DesignItemComponent {
  @Input() designItem!: Design;
  user$: Observable<User | null>;

  @Output() tagClicked = new EventEmitter<string>();

  constructor(
    private designServce: DesignService,
    private authService: AuthService,
  ) {
    this.user$ = this.authService.userFetched$;
  }

  getThumbnailLink(): string {
    if (this.designItem.thumbnailDataUrl) {
      return this.designItem.thumbnailDataUrl;
    } else if (this.designItem.id === 0) {
      return '/assets/no-thumb.png';
    }
    const thumb = this.designItem.thumbnailFile;
    if (thumb) {
      if (isValidUrl(thumb)) {
        return toHttps(thumb);
      } else {
        return `/static/${this.designItem.thumbnailFile}`;
      }
    }
    return '/assets/no-thumb.png';
  }

  getDownloadLink(): string {
    return `${environment.apiUrl}/designs/${this.designItem.id}/download`;
  }

  isYourDesign(user: User | null): boolean {
    return user !== null && this.designItem.creator.id === user.id;
  }

  trackByFn(index: number, item: Tag): string {
    return item.name; // or item.id
  }

  canDelete(user: User | null): boolean {
    if (user) {
      if (user.id === this.designItem.creator.id) {
        return true;
      }
      if (user.role === 'admin' || user.role === 'owner') {
        return true;
      }
    }
    return false;
  }

  delete(): void {
    this.designServce.deleteDesign(this.designItem);
  }
}
