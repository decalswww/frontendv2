import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesignGridComponent } from './design-grid/design-grid.component';
import { DesignEditorComponent } from './design-editor/design-editor.component';
import { AuthenticatedGuard } from '../auth/authenticated.guard';
import { DesignJsonCreatorComponent } from './design-json-creator/design-json-creator.component';

const routes: Routes = [
  {
    path: 'list',
    component: DesignGridComponent,
  },
  {
    path: 'json',
    component: DesignJsonCreatorComponent,
  },
  {
    path: 'form',
    component: DesignEditorComponent,
    canActivate: [AuthenticatedGuard],
  },
  {
    path: ':id/edit',
    component: DesignEditorComponent,
    canActivate: [AuthenticatedGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DesignRoutingModule {}
