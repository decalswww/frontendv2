import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import Design from 'src/app/core/interfaces/design.interface';
import { HttpClient } from '@angular/common/http';
import { DesignService } from '../design.service';
import { listStagger } from '../../core/animation/list-stagger.animation';
import {
  FilterDto,
  FilterFormComponent,
} from '../filter-form/filter-form.component';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ScrollService } from '../../core/services/scroll.service';

@Component({
  selector: 'app-design-grid',
  templateUrl: './design-grid.component.html',
  styleUrls: ['./design-grid.component.css'],
  animations: [listStagger],
})
export class DesignGridComponent implements OnInit, AfterViewInit {
  designs$: Observable<Design[] | null>;
  fetching$: Observable<boolean>;
  @ViewChild(FilterFormComponent) filterForm?: FilterFormComponent;

  constructor(
    private http: HttpClient,
    private designService: DesignService,
    private route: ActivatedRoute,
    private router: Router,
    private scrollServce: ScrollService,
  ) {
    this.designs$ = this.designService.designs$.asObservable();
    this.fetching$ = this.designService.fetching$.asObservable();
  }
  ngAfterViewInit(): void {
    this.route.queryParams.subscribe((params) => {
      const searchFilter: FilterDto = this.getFilterDto(params);
      if (this.filterForm) {
        this.filterForm.overrideTag(searchFilter.tagSearch);
      }
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      const searchFilter: FilterDto = this.getFilterDto(params);
      this.designService.getDesignsFiltered(searchFilter);
    });
  }

  getFilterDto(params: Params): FilterDto {
    return {
      search: params.search || '',
      tagSearch: params.tag || '',
      offset: params.offset || '0',
      limit: params.limit || '12',
      sort: params.sort || 'created:ASC',
    };
  }

  trackByMethod(index: number, design: Design): number {
    return design.id;
  }

  addQueryParams(params: Record<string, string>): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: params,
      queryParamsHandling: 'merge',
      replaceUrl: true,
    });
  }

  searchChanged(search: string): void {
    this.addQueryParams({ search, offset: '0' });
  }

  tagChanged(tag: string): void {
    this.addQueryParams({ tag, offset: '0' });
  }

  offsetChanged(offset: number): void {
    this.addQueryParams({ offset: offset.toString() });
  }

  limitChanged(limit: number): void {
    this.addQueryParams({ limit: limit.toString() });
  }

  changePage(change: number): void {
    const currentPage = +this.route.snapshot.queryParams.offset || 0;
    const newPage = currentPage + change;
    this.offsetChanged(newPage >= 0 ? newPage : 0);
    this.scrollServce.scroll(0, 0);
  }
}
