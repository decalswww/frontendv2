export enum Provider {
  GOOGLE = 'google',
  DISCORD = 'discord',
}

export interface User {
  id: number;
  name: string;
  avatar: string;
  role: 'editor' | 'admin' | 'owner';
}
