export default interface Design {
  id: number;

  name: string;

  description: string;

  creator: {
    id: number;
    name: string;
    avatar: string;
  };

  downloads: number;

  zipFile?: string;

  thumbnailFile?: string;
  thumbnailDataUrl?: string;
  tags: { name: string; id: number }[];

  date: number;
}
