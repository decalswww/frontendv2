import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeService } from './services/theme.service';
import { AuthService } from './services/auth.service';
import { ScrollService } from './services/scroll.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [ThemeService, AuthService, ScrollService],
})
export class CoreModule {}
