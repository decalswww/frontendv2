import {
  trigger,
  animate,
  transition,
  style,
  query,
  stagger,
  keyframes,
} from '@angular/animations';

export const listStagger = trigger('listStagger', [
  // Transition from any state to any state
  transition('* => *', [
    // Initially the all cards are not visible
    query(':enter', style({ opacity: 0 }), { optional: true }),

    query(
      ':enter',
      stagger('50ms', [
        animate(
          '.25s ease-out',
          keyframes([
            style({ opacity: 0, transform: 'translateY(+50%)', offset: 0 }),
            // style({
            //   opacity: 0.5,
            //   transform: 'translateY(+10px) scale(1.1)',
            //   offset: 0.3,
            // }),
            style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
          ]),
        ),
      ]),
      { optional: true },
    ),

    query(
      ':leave',
      stagger('33ms', [
        animate(
          '.25s ease-in',
          keyframes([
            style({ opacity: 1, transform: 'scale(1.1)', offset: 0 }),
            style({ opacity: 0.5, transform: 'scale(.5)', offset: 0.3 }),
            style({ opacity: 0, transform: 'scale(0)', offset: 1 }),
          ]),
        ),
      ]),
      { optional: true },
    ),
  ]),
]);
