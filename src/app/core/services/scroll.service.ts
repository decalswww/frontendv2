import { Injectable } from '@angular/core';

@Injectable()
export class ScrollService {
  private scrollElement?: Element;

  setScrollingElement(element: Element): void {
    this.scrollElement = element;
  }

  scroll(x: number, y: number, smooth: boolean = true): void {
    if (smooth) {
      this.scrollElement?.scroll({ left: x, top: y, behavior: 'smooth' });
    } else {
      this.scrollElement?.scroll({ left: x, top: y });
    }
  }
}
