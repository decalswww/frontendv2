import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { User } from '../interfaces/user.interface';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  private jwtToken = '';

  private user?: User;

  userFetched$ = new BehaviorSubject<User | null>(null);

  constructor(private httpClient: HttpClient, private router: Router) {
    this.load();
  }

  getToken(): string {
    return this.jwtToken;
  }

  getUserId(): number {
    if (this.user) {
      return this.user.id;
    } else {
      return 0;
    }
  }

  login(jwt: string): void {
    this.jwtToken = jwt;
    this.fetchUser();
    this.save();
  }

  logout(): void {
    this.jwtToken = '';
    this.clear();
    this.userFetched$.next(null);
    this.router.navigate(['/']);
  }

  getUser(): User | undefined {
    return this.user;
  }

  fetchUser(): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.jwtToken}`,
    });
    this.httpClient
      .get<User>(`${environment.apiUrl}/user`, { headers })
      .subscribe(
        (user) => {
          this.user = user;
          this.userFetched$.next(this.user);
        },
        () => {
          this.clear();
        },
      );
  }

  save(): void {
    localStorage.setItem('token', this.jwtToken);
  }

  clear(): void {
    localStorage.removeItem('token');
  }

  load(): void {
    const token = localStorage.getItem('token');
    if (token) {
      this.login(token);
    }
  }
}
