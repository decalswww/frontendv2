import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ThemeService {
  private isDarkTheme = false;
  private darkThemeSubject: BehaviorSubject<boolean>;
  darkThemeObservable: Observable<boolean>;

  constructor() {
    this.loadSetting();
    this.darkThemeSubject = new BehaviorSubject(this.isDarkTheme);
    this.darkThemeObservable = this.darkThemeSubject.asObservable();
  }

  toggleDarkTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
    this.darkThemeSubject.next(this.isDarkTheme);
    this.saveSetting();
  }

  setDarkTheme(isDarkTheme: boolean): void {
    this.isDarkTheme = isDarkTheme;
    this.darkThemeSubject.next(isDarkTheme);
    this.saveSetting();
  }

  saveSetting(): void {
    localStorage.setItem('darkTheme', this.isDarkTheme ? 'true' : 'false');
  }

  loadSetting(): void {
    this.isDarkTheme =
      localStorage.getItem('darkTheme') === 'true' ? true : false;
  }
}
