import { Component } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/core/interfaces/user.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-toolbar-profile',
  templateUrl: './toolbar-profile.component.html',
  styleUrls: ['./toolbar-profile.component.css'],
})
export class ToolbarProfileComponent {
  user$: Observable<User | null>;

  constructor(private authService: AuthService) {
    this.user$ = this.authService.userFetched$.asObservable();
  }

  logout(): void {
    this.authService.logout();
  }
}
