import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { ThemeService } from '../core/services/theme.service';
import { RouterOutlet, ActivatedRoute } from '@angular/router';
import { fadeAnimation } from '../core/animation/fade.animation';
import { AuthService } from '../core/services/auth.service';
import { User } from '../core/interfaces/user.interface';
import { ScrollService } from '../core/services/scroll.service';
import { MatSidenavContainer, MatSidenav } from '@angular/material/sidenav';
import { ElementRef } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  animations: [fadeAnimation],
})
export class ToolbarComponent implements OnInit, OnDestroy, AfterViewInit {
  isDarkTheme!: Observable<boolean>;
  mobileQuery: MediaQueryList;
  user$: Observable<User | null>;
  private _mobileQueryListener: () => void;
  @ViewChild(MatSidenavContainer) sidenavContainer!: MatSidenavContainer;

  constructor(
    private themeService: ThemeService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private authService: AuthService,
    private scrollService: ScrollService,
  ) {
    this.user$ = this.authService.userFetched$;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = (): void => {
      return changeDetectorRef.detectChanges();
    };
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngAfterViewInit(): void {
    if (this.sidenavContainer) {
      this.scrollService.setScrollingElement(
        this.sidenavContainer.scrollable.getElementRef().nativeElement,
      );
    }
  }

  ngOnInit(): void {
    this.isDarkTheme = this.themeService.darkThemeObservable;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  toggleDarkTheme(): void {
    this.themeService.toggleDarkTheme();
  }

  public getRouterOutletState(outlet: RouterOutlet): ActivatedRoute | string {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
