import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Material modules
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

import { ToolbarComponent } from './toolbar.component';
import { AppRoutingModule } from '../app.routing';
import { ToolbarProfileComponent } from './toolbar-profile/toolbar-profile.component';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
  ],
  declarations: [ToolbarComponent, ToolbarProfileComponent],
  exports: [ToolbarComponent],
})
export class ToolbarModule {}
