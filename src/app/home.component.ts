import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { listStagger } from './core/animation/list-stagger.animation';
import Design from './core/interfaces/design.interface';
import { DesignService } from './design/design.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  animations: [listStagger],
})
export class HomeComponent {
  top5$: Observable<Design[] | null>;
  last5$: Observable<Design[] | null>;

  constructor(private designService: DesignService) {
    designService.fetchLast5();
    designService.fetchTop5();

    this.top5$ = this.designService.top5$.asObservable();
    this.last5$ = this.designService.last5$.asObservable();
  }

  trackByMethod(index: number, design: Design): number {
    return design.id;
  }
}
